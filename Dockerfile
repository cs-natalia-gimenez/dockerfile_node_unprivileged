## Defina a versao desejada do Node.
# Testado apenas com as imagens oficiais!
FROM node:6.2.0
MAINTAINER Natalia Gimenez <natalia.gimenez@concrete.com.br>

## Adiciona um usuario para executar o node
RUN useradd -m node -u 1000 --user-group -G 100 -s /bin/bash -d /home/node
USER node

## Cria um diretorio local onde os pacotes serao instalados
RUN mkdir /home/node/.npm-packages

## Adiciona as variaveis para que os pacotes sempre sejam instalados
# no home do usuario node, mesmo quando o -g (global) for utilizado
RUN echo 'prefix = /home/node/.npm-packages' > /home/node/.npmrc
ENV PATH "/home/node/.npm-packages/bin:$PATH"
ENV NPM_PACKAGES '/home/node/.npm-packages'
